# python_ffmpeg_mysql

Die Anwendung soll Videos transcodieren.
Die Videos landen auf einem Fileserver. Dort soll ein File System Watcher beobachten, wenn ein File hinzukommt.

Ein anderer Teil der Anwendung transcodiert die Videos. Entweder automatisch oder per Auftrag. (

Welche Videos wie transcodiert werden sollen, muss wieder ein Anderer Teil der Applikation entscheiden. Er sendet den Auftrag mit Parametern etc. an eine zentrale Datenbank.

Ein Job Runner, holt sich einen Auftrag ab und transcodiert das Video.

## Der Workflow:
- Ein Video wird in den Filestore hochgeladen.
- Der Filewatcher erkennt das neue File und meldet per Http-request an den api-server (manchmal db_server genannt), dass ein neues File vorhanden ist. Es wird mit einer einzigartigen ID in der Datenbank gespeichert.
- Ein Client (Benutzer der Weboberfläche - nur hypothetisch. Wird nicht programmiert) kann sich die registrierten Files anzeigen und einen Job starten um ein File zu transcodieren.
- Es wird ein neuer Job in der Datenbank erzeugt, der folgende Komponenten enthält:
  * das Kommando, mit dem der Film transcodiert wird (in etwa so: "ffmpeg -in mymovie -out mymovie.avi")
  * Informationen zum Jobstatus: Erstelldatum, Status (inprogress, done, failed)
- ein Job Runner - Programm, dass die eigentliche Arbeit (das Transcoding) erledigt, fragt periodisch die Datenbank ab, ob ein neuer Job vorliegt. Falls ja, wird der Runner den Job annehmen und mit dem Transcodieren beginnen.
  * Damit nicht ein und der selbe Job gleichzeitig von zwei Jobrunnern abgerufen werden kann, wird auf Datenbankebene ein Lock erteilt und der Status des Jobs auf `inprocess` gesetzt.
  * sobald der Job fertig ist, meldet der Runner dies an die DB und der Jobstatus wird auf `done` gesetzt.
  * um zu vermeiden, dass ein Video nicht transcodiert wird, falls ein Jobrunner abstürzt etc., ist ein check-in-Mechanismus implementiert: der Runner muss alle paar sekunden ein Lebenszeichen senden. Wenn dieses Ausbleibt, nimmt der Jobmanager an, dass der Job gescheitert ist und setzt ihn auf `failed`

## DB-API: Endpunkte
### /resources/files
*Fileobjekte abfragen oder anlegen*
Erlaubte Methoden: GET, POST

```
Bsp: GET /resources/files?id=1

[
  {
    "date_added": "Fri, 28 Aug 2020 13:53:23 GMT", 
    "id": 1, 
    "name": "Mulan"
  }
]
```
*neues File registrieren mit POST:

```
Bsp: POST /resources/files

  {
    "name": "Avatar"
  }
```
### /resources/files/all
Erlaubte Methoden: GET, POST
### /resources/jobs
Erlaubte Methoden: GET, POST
### /resources/jobs/all
Erlaubte Methoden: GET
### /resources/jobs/next
Erlaubte Methoden: GET
### /resources/jobs/done
*einen Job als ausgeführt markieren*
Erlaubte Methoden: POST
```
Bsp: POST /resources/jobs/done
  {
    "id": 1,
    "status": "done"
  }

```
## filewatch
Die Komponente filewatch überwacht ein definiertes Verzeichnis daraufhin, ob ein neues File erstellt wird. Ist das der Fall, wird per API-Call an den api-server das neue File registriert.
Das zu überwachende Verzeichnis sowie die Url des Api-servers lassen sich per Environmentvariable setzen.
`FILEWATCH_PATH` und `API_SERVER_URL`.
Falls diese Variablen nicht gesetzt sind, wird als default "/video/input" und "http://127.0.0.1:5000" genutzt.

## transcoder / worker
Der Transcoder ist die Softwarekomponente, die die Umrechnung der Videos erledigt. Er holt sich einen Auftrag/Job vom Api-Server.
Sobald ein Job abgeholt wird, wird er in der Datenbank als `inprocess` gesetzt.

Der Transcoder ruft dann den Subprocess `ffmpeg` auf. Sobald ffmpeg fertig ist mit dem Transcodieren, wird ein `status: done` an den Api-server gesendet.

## ENVIRONMENT VARIABLES
    DB_BACKEND_HOST,
    DB_BACKEND_USERNAME,
    DB_BACKEND_PASSWORD,
    DB_BACKEND_DB_NAME,
    DB_BACKEND_PORT,