# python_ffmpeg_mysql
## 00_plain_sourcecode
Herkömmliche/offensichtliche Variante:
Der Sourcecode liegt auf dem Dateisystem des Entwicklerrechners.
Der Entwickler installiert alle Abhängigkeiten, Pakete usw. auf seinem Rechner.

Vorteil: 
- einfach zu Verstehen
- reibungsloses Entwickeln da keine Medienbrüche

Nachteil:
- das Produktivsystem sieht anders aus als das Macbook des Entwicklers. Es kommt beim Productiondeployment häufig zu Problemen.
- alle Abhängigkeiten müssen lokal installiert sein, sie können Probleme mit anderen Projekten verursachen -> dagegen helfen Python virtual environments.


## Mariadb installieren mit Docker
Die Anwendung benötigt eine Backenddatenbank.
Je nach Betriebssystem lässt sich mysql/mariadb aus den Paketquellen installieren.
Da ich in meiner Entwicklungsumgebung (WSL2) keinen Mysqlserver installieren möchte umschiffe ich das Problem durch Nutzung eines Dockercontainers. - Das ist ein Vorgriff auf die folgenden Kapitel, in denen Docker explizit eingeführt wird.

## Python dependencies mit venv installieren
```
apt-get install python3-venv
python3 -m venv venv
. venv/bin/activate
pip3 install -r path/to/requirements.txt
```

## Anwendung testen mit curl
Die zentrale Komponente der Anwendung ist ein API-Server, der REST-Befehle entgegennimmt und daraufhin mit der Datenbank interagiert.
Die REST-API lässt sich mit curl oder z.B. Postman testen:
```
curl http://localhost:5000/resources/files/all
curl http://localhost:5000/resources/files?id=1

# es lässt sich eine Auflistung aller jobs erzeugen:
curl http://localhost:5000/resources/jobs/all
# Response:
[
  {
    "command": "ffmpeg -i \"Heuschreckenalarm 2 - Sie schrecken wieder!\" \"Heuschreckenalarm 2 - Sie schrecken wieder!.avi\" ",
    "date_added": "Fri, 28 Aug 2020 13:54:23 GMT",
    "done": 0,
    "file_id": 2,
    "id": 1,
    "inprocess": 1
  },
  {
    "command": "ffmpeg -i \"Molignan\" \"Molignan.avi\" ",
    "date_added": "Fri, 28 Aug 2020 13:56:03 GMT",
    "done": 0,
    "file_id": 1,
    "id": 2,
    "inprocess": 1
  },
  {
    "command": "ffmpeg -i \"Mephisto\" \"Mephisto.avi\" ",
    "date_added": "Fri, 28 Aug 2020 13:56:59 GMT",
    "done": 1,
    "file_id": 3,
    "id": 3,
    "inprocess": 0
  }
]

```


## Einrichtung der Datenbank, wenn kein Dockercontainer genutzt wird:
## Datenbank initialisieren
Die Datenbank muss mit einem Dump von der Kommandozeile eingespielt werden.

### User anlegen:
Ein anderer User als root muss angelegt werden.
```
mysql -uroot
create user python;
GRANT ALL PRIVILEGES on *.* TO 'python'@'%';
ALTER USER 'python'@'localhost' IDENTIFIED BY 'password';
exit
```
### Datenbank einspielen:
```
mysql -upython -ppassword < sqlxxxxxx.sql
```
Das Datenbankschema ist jetzt erstellt.