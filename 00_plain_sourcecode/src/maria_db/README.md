# Datenbank mit Docker initialisieren
man kann die Backendatenbank auch als befüllten Dockercontainer starten.

# Docker installieren in Ubuntu

Ubuntu 20 benutzt docker snap. Das wird deinstalliert.

```
sudo apt-get remove docker docker-engine docker.io containerd runc

apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose

# Ubuntu 20 benutzt snap für docker.
snap remove docker
systemctl restart docker

```


### Image bauen:
```
docker build -f mariadb.dockerfile -t testingdb .
```

### Container mit Docker-Compose starten
```
cd docker/maria
docker-compose up -d
```
