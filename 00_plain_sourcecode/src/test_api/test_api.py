import json
import os, sys
import pytest
import requests


API_SERVER = os.getenv("API_SERVER", "127.0.0.1")
API_SERVER_PORT = os.getenv("API_SERVER_PORT", "5000")
API_SERVER_URL = "http://" + API_SERVER + ":" + API_SERVER_PORT


test_welcome_screen = {"method": "GET", "url": API_SERVER_URL}
test_files_all = {"method": "GET", "url": API_SERVER_URL + "/resources/files/all"}
test_files = {"method": "GET", "url": API_SERVER_URL + "/resources/files?id=1"}


def test_welcome_screen_status_code():
    req = requests.request(**test_welcome_screen)
    assert req.status_code == 200


def test_welcome_screen_content():
    expected = b"<h1>Welcome to Distributed Transcoder</h1><p>Have fun</p>"
    req = requests.request(**test_welcome_screen)
    assert req.content == expected


def test_files_all_status_code():
    req = requests.request(**test_files_all)
    assert req.status_code == 200


def test_files_all_content():
    expected = {
        "date_added": "Fri, 28 Aug 2020 13:53:23 GMT",
        "id": 1,
        "name": "Molignan",
    }
    req = requests.request(**test_files_all)
    req = json.loads(req.content)
    assert req[0] == expected


def test_file_id_is_one_status_code():
    req = requests.request(**test_files)
    req = req.status_code
    assert req == 200


def test_file_id_one_content():
    expected = {
        "date_added": "Fri, 28 Aug 2020 13:53:23 GMT",
        "id": 1,
        "name": "Molignan",
    }
    req = requests.request(**test_files)
    req = json.loads(req.content)
    assert req[0] == expected


def test_insert_file_named_froschkoenig():
    payload = {"name": "froschkoenig"}

    req = requests.post(
        API_SERVER_URL + "/resources/files?id=1",
        json=payload,
    )
    assert req.status_code == 201


def test_insert_file_without_name_fails():
    payload = {"id": "froschkoenig"}

    req = requests.post(
        API_SERVER_URL + "/resources/files",
        json=payload,
    )
    assert req.status_code != 201


def test_jobs_all_status_code():
    req = requests.get(API_SERVER_URL + "/resources/jobs/all")
    assert req.status_code == 200


def test_jobs_all_content():
    expected = {
        "command": 'ffmpeg -i "Heuschreckenalarm 2 - Sie schrecken wieder!" "Heuschreckenalarm 2 - Sie schrecken wieder!.avi" ',
        "date_added": "Fri, 28 Aug 2020 13:54:23 GMT",
        "done": 0,
        "failed": 0,
        "file_id": 2,
        "id": 1,
        "inprocess": 1,
        "last_check_in": None,
    }
    req = requests.get(API_SERVER_URL + "/resources/jobs/all")
    req = json.loads(req.content)
    assert req[0] == expected


def test_jobs_id_equals_one_content():
    expected = {
        "command": 'ffmpeg -i "Heuschreckenalarm 2 - Sie schrecken wieder!" "Heuschreckenalarm 2 - Sie schrecken wieder!.avi" ',
        "date_added": "Fri, 28 Aug 2020 13:54:23 GMT",
        "done": 0,
        "failed": 0,
        "file_id": 2,
        "id": 1,
        "inprocess": 1,
        "last_check_in": None,
    }
    req = requests.get(API_SERVER_URL + "/resources/jobs?id=1")
    req = json.loads(req.content)
    assert req[0] == expected
