#!/bin/bash

if pytest --junitxml ./results/results.xml ; then
    touch ./results/SUCCESS
else
    echo "Errors found by pytest"
fi
