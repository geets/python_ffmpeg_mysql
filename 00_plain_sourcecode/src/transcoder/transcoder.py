import subprocess
import json
import logging
import threading
import time
import os, sys

sys.path.append(os.path.dirname(sys.path[0]))
from api_client import api_client


logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)

video_directory = "~/video/"

API_SERVER = os.getenv("API_SERVER", "127.0.0.1")

API_SERVER_PORT = os.getenv("API_SERVER_PORT", "5000")

API_SERVER_URL = "http://" + API_SERVER + ":" + API_SERVER_PORT
logging.info("api server at {}".format(API_SERVER_URL))

apicall = api_client.ApiCall(API_SERVER_URL)


def transcode_file(job):
    file_name = job.json()["command"]
    file_path = video_directory + file_name
    command = "ffmpeg -y -i {} {}.avi".format(file_path, file_path)
    logging.info("command: {}".format(command))
    result = subprocess.run(command, shell=True)
    return result


def main(next_job):
    if next_job.status_code == 200:
        result = transcode_file(next_job)
        if result.returncode == 0:
            apicall.job_done(json.loads(next_job.content)["id"])
    else:
        print(next_job.status_code)


def check_in(job_id):
    response = apicall.job_running(job_id)
    logging.info("checking in job id={} - {}".format(job_id, response.status_code))


next_job = apicall.get_next_job()
next_job_id = json.loads(next_job.content)["id"]

main_thread = threading.Thread(target=main, args=(next_job,))
main_thread.start()

while main_thread.is_alive():
    check_in(next_job_id)
    time.sleep(5)
