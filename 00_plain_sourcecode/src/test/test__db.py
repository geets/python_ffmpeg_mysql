import os
import sys
from unittest.mock import Mock

sys.path.append(os.path.dirname(sys.path[0]))
from db_server import database, file
import pytest

# myReadWrite = database.ReadWriteQuery(mydb)
# file_id1 = myReadWrite.execute_read_query("SELECT * FROM file WHERE id=1")


def test_file_name_by_id_returns_name():
    expected = {
        "date_added": "Fri, 28 Aug 2020 13:53:23 GMT",
        "id": 1,
        "name": "Molignan",
    }
    mock_connection = Mock()
    mock_connection.execute_query = Mock(return_value=[expected])
    myFileReadWrite = file.FileReadWrite(mock_connection)
    result = myFileReadWrite.get_file_name_by_id(1)
    assert result == "Molignan"

def test_file_name_by_id_returns_none_if_wrong_id():
    expected = None

#
#
# def test_get_all_files_returns_3():
#    FileReadWrite = file.FileReadWrite(myReadWrite)
#    files = FileReadWrite.get_all()
#    assert len(files) == 3
#
#
# def test_get_file_name_by_id_gives_correct_name():
#    FileReadWrite = file.FileReadWrite(myReadWrite)
#    filename = FileReadWrite.get_file_name_by_id(1)
#    assert filename == "Molignan"
#