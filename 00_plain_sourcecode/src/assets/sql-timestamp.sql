-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema transcoder
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema transcoder
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `transcoder` DEFAULT CHARACTER SET utf8 ;
USE `transcoder` ;

-- -----------------------------------------------------
-- Table `transcoder`.`file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transcoder`.`file` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_added` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `transcoder`.`job`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transcoder`.`job` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `file_id` INT NOT NULL,
  `done` TINYINT NULL DEFAULT 0,
  `command` VARCHAR(1000) NOT NULL,
  `inprocess` TINYINT NULL DEFAULT 0,
  `date_added` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`file_id` ASC),
  CONSTRAINT `id`
    FOREIGN KEY (`file_id`)
    REFERENCES `transcoder`.`file` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
