import requests
import logging


logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)

url = "http://127.0.0.1:5000"


class ApiCall:
    def __init__(self, api_server):
        self.api_server = api_server
        self.path_to_files = "/resources/files"
        self.path_to_jobs = "/resources/jobs"
        logging.info(
            "set api endpoints to:\n{}\n{}".format(
                self.path_to_files, self.path_to_jobs
            )
        )

    def get_job(self, id="all"):
        if id == "all":
            url = self.api_server + self.path_to_jobs + "/all"
        else:
            query_param = "?id=" + str(id)
            url = self.api_server + self.path_to_jobs + query_param
        response = requests.get(url)
        return response

    def get_next_job(self):
        url = self.api_server + self.path_to_jobs + "/next"
        response = requests.get(url)
        return response

    def get_file(self, id="all"):
        if id == "all":
            url = self.api_server + self.path_to_files + "/all"
        else:
            query_param = "?id=" + str(id)
            url = self.api_server + self.path_to_files + query_param
        response = requests.get(url)
        return response

    def add_file(self, name):
        url = self.api_server + self.path_to_files
        body = {"name": name}
        response = requests.post(url, json=body)
        return response

    def add_job(self, file_id):
        url = self.api_server + self.path_to_jobs
        body = {"id": file_id}
        response = requests.post(url, json=body)
        return response

    def job_done(self, job_id):
        url = self.api_server + self.path_to_jobs
        body = {"id": job_id, "status": "done"}
        response = requests.patch(url, json=body)
        return response

    def job_failed(self, job_id):
        url = self.api_server + self.path_to_jobs
        body = {"id": job_id, "status": "failed"}
        response = requests.patch(url, json=body)
        return response

    def job_running(self, job_id):
        url = self.api_server + self.path_to_jobs
        body = {"id": job_id, "status": "running"}
        response = requests.patch(url, json=body)
        return response