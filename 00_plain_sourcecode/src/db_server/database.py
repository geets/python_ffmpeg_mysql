import mysql.connector
from mysql.connector import Error
import logging

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)


class DatabaseConnection:
    def __init__(self, host_name, user_name, user_password, data_base, port=3306):
        self.connection = None
        self.cursor = None

        try:
            self.connection = mysql.connector.connect(
                host=host_name,
                user=user_name,
                passwd=user_password,
                database=data_base,
                port=port,
                autocommit=True,
            )
            self.cursor = self.connection.cursor(dictionary=True)
            logging.info("database connection established")

        except Error as e:
            logging.error(e)


class ReadWriteQuery:
    def __init__(self, connection):
        self.connection = connection

    def execute_query(self, query):
        try:
            logging.info("query successfull: {}".format(query))
            self.connection.cursor.execute(query)
            return self.connection.cursor.fetchall()
        except Error as e:
            logging.info(e)


