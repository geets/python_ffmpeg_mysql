import logging

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)


class JobReadWrite:
    def __init__(self, connection):
        self.connection = connection
        self.query_lowest_job_id = """
        SELECT * FROM job WHERE inprocess=0 AND done=0 AND failed=0 ORDER BY id ASC LIMIT 0, 1;
        """

        self.query_lowest_job_id_and_lock = """
        SELECT * FROM job WHERE inprocess=0  ORDER BY id ASC LIMIT 0, 1 FOR UPDATE;
        """
        self.jobs_being_processed = []

    # If no next job available, return a message. This will handed down the line up to the webserver
    def get_lowest_id_job_and_lock(self):
        self.lowest_id_job = self.connection.execute_query(self.query_lowest_job_id)
        if self.lowest_id_job:
            logging.info(
                "returning job with lowest id: id={}".format(self.lowest_id_job[0])
            )
            return self.lowest_id_job[0]
        if not self.lowest_id_job:
            logging.info("no next job available")
            return "NO NEXT JOB"

    def mark_job_as_inprocess(self, id):
        self.query_mark_as_inprocess = """
        UPDATE job SET inprocess=1 WHERE id={};
        """.format(
            id
        )
        self.connection.execute_query(self.query_mark_as_inprocess)
        logging.info("marking job id={} as inprocess".format(id))

    def mark_job_as_done(self, id):
        self.query_mark_as_done = """
        UPDATE job SET inprocess=0, failed=0, done=1 WHERE id={};
        """.format(
            id
        )
        self.connection.execute_query(self.query_mark_as_done)
        logging.info("marking job id={} as done".format(id))

    def mark_job_as_failed(self, id):
        self.query_mark_as_failed = """
        UPDATE job SET inprocess=0, failed=1, done=0 WHERE id={};
        """.format(
            id
        )
        self.connection.execute_query(self.query_mark_as_failed)
        logging.info("marking job id={} as failed".format(id))

    def mark_job_as_running(self, id):
        self.query_set_timestamp = """
        UPDATE job SET last_check_in=CURRENT_TIMESTAMP, inprocess=1, failed=0, done=0 WHERE id={};
        """.format(
            id
        )
        self.connection.execute_query(self.query_set_timestamp)
        logging.info("check-in for job id={}".format(id))

    def get_job_by_id(self, job_id):
        self.query_job_by_id = """
        SELECT * FROM job WHERE id={};
        """.format(
            job_id
        )
        self.job = self.connection.execute_query(self.query_job_by_id)
        logging.info("getting job with id={}".format(job_id))
        return self.job

    def create_job_by_file_id(self, file_id, FileReadWrite):
        self.file_name = FileReadWrite.get_file_name_by_id(file_id)
        self.command = self.file_name
        self.job = (file_id, self.command)
        logging.info("creating job for file with id={}".format(file_id))
        return self.job

    def get_next_job(self):
        self.job = self.get_lowest_id_job_and_lock()
        try:
            self.mark_job_as_inprocess(self.job["id"])
            logging.info("returning next new job: id={}".format(self.job["id"]))
            return self.job
        except:
            return self.job

    def insert_into_job(self, job):
        self.query_insert_into_table_job = """
        INSERT INTO job
        (file_id, command)
        VALUES
        ('{}', '{}') ;
        """.format(
            job[0], job[1]
        )
        self.connection.execute_query(self.query_insert_into_table_job)
        logging.info(" job: id={}".format(self.job[0]))

    def get_all(self):
        self.query_get_all = """ SELECT * FROM job; """
        return self.connection.execute_query(self.query_get_all)