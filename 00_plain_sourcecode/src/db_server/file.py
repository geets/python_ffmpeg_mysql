import json
import logging

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)


class FileReadWrite:
    def __init__(self, connection):
        self.query_select_file = "SELECT * FROM file"
        self.connection = connection

    def get_file_name_by_id(self, file_id):
        self.query_file_by_id = """
        SELECT name FROM file WHERE id={};
        """.format(
            file_id
        )
        self.file = self.connection.execute_query(self.query_file_by_id)
        logging.info("get name of file with id={}".format(file_id))
        return self.file[0]["name"]

    def get_file_by_id(self, file_id):
        self.query_file_by_id = """
        SELECT * FROM file WHERE id={};
        """.format(
            file_id
        )
        self.file = self.connection.execute_query(self.query_file_by_id)
        logging.info("get file with id={}".format(file_id))
        return self.file

    def insert_into_file(self, file_name):
        self.query_insert_into_table_file = """
        INSERT INTO file
        (name)
        VALUES
        ('{}') ;
        """.format(
            file_name
        )
        self.connection.execute_query(self.query_insert_into_table_file)
        logging.info("adding file to database: {}".format(file_name))

    def get_all(self):
        """
        returns all files
        returntype: list
            list length is number of files in DB.
        """
        self.query_get_all = """ SELECT * FROM file; """
        logging.info("returning all files")
        return self.connection.execute_query(self.query_get_all)
