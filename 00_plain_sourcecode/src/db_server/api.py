"""
Flask based API-Server that listenes to HTTP-Requests.
"""
import os
import logging
import flask
from flask import jsonify, request
import job
import database
import file

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)


app = flask.Flask(__name__)
app.config["DEBUG"] = True


API_LISTEN_IP = os.getenv("API_LISTEN_IP", "0.0.0.0")
API_LISTEN_PORT = os.getenv("API_LISTEN_PORT", "5000")
DB_BACKEND_HOST = os.getenv("DB_BACKEND_HOST", "localhost")
DB_BACKEND_PORT = os.getenv("DB_BACKEND_PORT", "3306")
DB_BACKEND_DB_NAME = os.getenv("DB_BACKEND_DB_NAME", "transcoder")
DB_BACKEND_USERNAME = os.getenv("DB_BACKEND_USERNAME", "python")
DB_BACKEND_PASSWORD = os.getenv("DB_BACKEND_PASSWORD", "password")

db_connection_parameters = (
    DB_BACKEND_HOST,
    DB_BACKEND_USERNAME,
    DB_BACKEND_PASSWORD,
    DB_BACKEND_DB_NAME,
    DB_BACKEND_PORT,
)


@app.route("/", methods=["GET"])
def home():
    """
    route / returns Greeting Screen
    """
    return "<h1>Welcome to Distributed Transcoder</h1><p>Have fun</p>"


# FILES:
@app.route("/resources/files/all", methods=["GET"])
def get_all_files():
    """
    returns all files
    returntype: list
        list length is number of files in DB.
    """
    myconnection = database.DatabaseConnection(*db_connection_parameters)
    myReadWriter = database.ReadWriteQuery(myconnection)
    myFileReadWriter = file.FileReadWrite(myReadWriter)
    all_files = myFileReadWriter.get_all()
    return jsonify(all_files)


@app.route("/resources/files", methods=["GET"])
def get_file_by_id():
    """
    takes a file_id and returns a file.
    """
    if "id" in request.args:
        id = int(request.args["id"])
        myconnection = database.DatabaseConnection(*db_connection_parameters)
        myReadWriter = database.ReadWriteQuery(myconnection)
        myFileReadWriter = file.FileReadWrite(myReadWriter)
        return jsonify(myFileReadWriter.get_file_by_id(id))
    return "wrong format"


@app.route("/resources/files", methods=["POST"])
def add_file_json():
    """
    takes a filename and creates a new file entry in the database.
    """
    req = request.get_json()
    myconnection = database.DatabaseConnection(*db_connection_parameters)
    myReadWriter = database.ReadWriteQuery(myconnection)
    myFileReadWriter = file.FileReadWrite(myReadWriter)
    myFileReadWriter.insert_into_file(req["name"])
    return "", 201


# JOBS:
@app.route("/resources/jobs/all", methods=["GET"])
def get_all_jobs():
    """
    returns all jobs in db.
    Browser will represent it like this: (a list with dict-like members)
        [
            {
                "command": "ffmpeg -i \"Heuschreckenalarm 2 - Sie schrecken wieder!\" \"Heuschreckenalarm 2 - Sie schrecken wieder!.avi\" ",
                "date_added": "Fri, 28 Aug 2020 13:54:23 GMT",
                "done": 0,
                "failed": 0,
                "file_id": 2,
                "id": 1,
                "inprocess": 1,
                "last_check_in": null
            }
        ]

    returntype: flask.response object.

    example:
    return jsonify(username=g.user.username,
                       email=g.user.email,
                       id=g.user.id)
    This will send a JSON response like this to the browser:

    {
        "username": "admin",
        "email": "admin@localhost",
        "id": 42
    }
    """
    myconnection = database.DatabaseConnection(*db_connection_parameters)
    myReadWriter = database.ReadWriteQuery(myconnection)
    myJobReadWriter = job.JobReadWrite(myReadWriter)
    all_jobs = myJobReadWriter.get_all()
    return jsonify(all_jobs)


@app.route("/resources/jobs", methods=["GET"])
def get_job_by_id():
    """
    takes a job_id and returns a json containing all information on that job.
    """
    if "id" in request.args:
        try:
            id = int(request.args.get("id"))
            myconnection = database.DatabaseConnection(*db_connection_parameters)
            myReadWriter = database.ReadWriteQuery(myconnection)
            myJobReadWriter = job.JobReadWrite(myReadWriter)
            return jsonify(myJobReadWriter.get_job_by_id(id))
        except ValueError:
            logging.error(
                "id accepts only int as input. {} was put in".format(
                    request.args.get("id")
                )
            )
            return "wrong format"
    else:
        return "wrong format"


@app.route("/resources/jobs", methods=["POST"])
def add_job_by_file_id_json():
    """
    takes a file_id and creates a new job in database, that handles this job with default
    parameters.
    """
    req = request.get_json()
    myconnection = database.DatabaseConnection(*db_connection_parameters)
    myReadWriter = database.ReadWriteQuery(myconnection)
    myJobReadWriter = job.JobReadWrite(myReadWriter)
    myFileReadWriter = file.FileReadWrite(myReadWriter)
    new_job = myJobReadWriter.create_job_by_file_id(int(req["id"]), myFileReadWriter)
    myJobReadWriter.insert_into_job(new_job)
    return "", 201


@app.route("/resources/jobs/next", methods=["GET"])
def get_next_job():
    """
    returns the next job that needs to be worked on.
    """
    myconnection = database.DatabaseConnection(*db_connection_parameters)
    myReadWriter = database.ReadWriteQuery(myconnection)
    myJobReadWriter = job.JobReadWrite(myReadWriter)
    next_job = myJobReadWriter.get_next_job()

    if "id" in next_job:
        return jsonify(next_job), 200
    return jsonify(next_job), 404


@app.route("/resources/jobs", methods=["PATCH"])
def change_job_status():
    """
    takes status: done/failed/running and sets jobstatus accordingly.
    """
    req = request.get_json()
    myconnection = database.DatabaseConnection(*db_connection_parameters)
    myReadWriter = database.ReadWriteQuery(myconnection)
    myJobReadWriter = job.JobReadWrite(myReadWriter)
    if req["status"] == "done":
        myJobReadWriter.mark_job_as_done(int(req["id"]))
    elif req["status"] == "failed":
        myJobReadWriter.mark_job_as_failed(int(req["id"]))
    elif req["status"] == "running":
        myJobReadWriter.mark_job_as_running(int(req["id"]))
    return "", 200


app.run(host=API_LISTEN_IP, port=API_LISTEN_PORT)
