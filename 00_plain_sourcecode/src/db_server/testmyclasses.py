import os
import logging
import flask
from flask import jsonify, request
from flask import json
import job
import database
import file


API_LISTEN_IP = os.getenv("API_LISTEN_IP", "0.0.0.0")
API_LISTEN_PORT = os.getenv("API_LISTEN_PORT", "5000")
DB_BACKEND_HOST = os.getenv("DB_BACKEND_HOST", "localhost")
DB_BACKEND_PORT = os.getenv("DB_BACKEND_PORT", "3306")
DB_BACKEND_DB_NAME = os.getenv("DB_BACKEND_DB_NAME", "transcoder")
DB_BACKEND_USERNAME = os.getenv("DB_BACKEND_USERNAME", "python")
DB_BACKEND_PASSWORD = os.getenv("DB_BACKEND_PASSWORD", "password")

db_connection_parameters = (
    DB_BACKEND_HOST,
    DB_BACKEND_USERNAME,
    DB_BACKEND_PASSWORD,
    DB_BACKEND_DB_NAME,
    DB_BACKEND_PORT,
)
# needed so jsonify works
app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.run(host=API_LISTEN_IP, port=API_LISTEN_PORT)

myconnection = database.DatabaseConnection(*db_connection_parameters)
print("--")
print("myconnection = database.DatabaseConnection(*db_connection_parameters)")
print("type myconnection:")
print(type(myconnection))
print("print('myconnection'):")
print(myconnection)
print(" ")

myReadWriter = database.ReadWriteQuery(myconnection)
print("--")
print("myReadWriter = database.ReadWriteQuery(myconnection)")
print("type myReadWriter:")
print(type(myReadWriter))
print("print('myReadWriter'):")
print(myReadWriter)
print(" ")
myFileReadWriter = file.FileReadWrite(myReadWriter)

all_files = myFileReadWriter.get_all()
jsonify_all_files = jsonify(all_files)
print("--")
print("all_files = myFileReadWriter.get_all()")
print("print type of all_files:")
print(type(all_files))
print("print len of all_files:")
print(len(all_files))
print(" ")
print("jsonify(all_files):")
print("type jsonify(all_files)")
print(type(jsonify_all_files))