import os
import sys

sys.path.append(os.path.dirname(sys.path[0]))
from api_client import api_client
import time
import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)
logging.info("starting filewatch")

if os.getenv("FILEWATCH_PATH"):
    FILEWATCH_PATH = os.getenv("FILEWATCH_PATH")
    logging.info("watching directory {}".format(FILEWATCH_PATH))
else:
    FILEWATCH_PATH = "/video/input"
    logging.info("watching directory {}".format(FILEWATCH_PATH))

if os.getenv("API_SERVER"):
    API_SERVER = os.getenv("API_SERVER")
else:
    API_SERVER = "127.0.0.1"
    logging.info("api server at {}".format(API_SERVER))

API_SERVER_PORT = os.getenv("API_SERVER_PORT", "5000")

API_SERVER_URL = "http://" + API_SERVER + ":" + API_SERVER_PORT
logging.info("api server at {}".format(API_SERVER_URL))


class Handler(FileSystemEventHandler):
    def on_created(self, event):
        ApiCall.add_file(os.path.basename(event.src_path))
        logging.info(
            "detected new file. sending add_file to api-server.\nFilename: {} ".format(
                event.src_path
            )
        )


if __name__ == "__main__":
    ApiCall = api_client.ApiCall(API_SERVER_URL)
    event_handler = Handler()
    observer = Observer()
    observer.schedule(event_handler, FILEWATCH_PATH, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    finally:
        observer.stop()
        observer.join()