import logging
import json
import time
import datetime
import os, sys

sys.path.append(os.path.dirname(sys.path[0]))
from api_client import api_client

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger(__name__)
logging.info("starting manager")

API_SERVER = os.getenv("API_SERVER", "127.0.0.1")

API_SERVER_PORT = os.getenv("API_SERVER_PORT", "5000")

API_SERVER_URL = "http://" + API_SERVER + ":" + API_SERVER_PORT
logging.info("api server at {}".format(API_SERVER_URL))


def get_all_jobs():
    ApiCall = api_client.ApiCall(API_SERVER_URL)
    all_jobs = ApiCall.get_job()
    return all_jobs


def set_job_failed(job_id):
    ApiCall = api_client.ApiCall(API_SERVER_URL)
    ApiCall.job_failed(job_id)
    logging.warning(
        "detected job as failed due to expired check-in time job_id={}".format(job_id)
    )


# https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
def get_age(time_string):
    if time_string == None:
        return datetime.timedelta(seconds=0)
    else:
        time_obj = datetime.datetime.strptime(time_string, "%a, %d %b %Y %H:%M:%S GMT")
        now = datetime.datetime.utcnow()
        difference = now - time_obj
        return difference


def set_stale_jobs_to_failed(jobs):  # jobs = responseObj
    agelimit = datetime.timedelta(seconds=30)
    parsed_jobs = json.loads(
        jobs.content
    )  # parsed_jobs = list, parsed_jobs[0] = dict, parsed_jobs[1] = dict
    for job in parsed_jobs:
        job_check_in = job["last_check_in"]
        age = get_age(job_check_in)
        state_failed = job["failed"]
        state_done = job["done"]
        if not state_done == 1 and not state_failed == 1:
            if age > agelimit:
                set_job_failed(job["id"])
            else:
                print("okay", job["id"])


while True:
    all_jobs = get_all_jobs()
    set_stale_jobs_to_failed(all_jobs)
    time.sleep(30)